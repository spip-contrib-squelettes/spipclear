<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'ajouter_commentaire' =>'Adicionar um coment&aacute;rio',
'archive' =>'Arquivo',
'archives' =>'Arquivos',
'attime' =>'em',
'aucune_archive' =>'Nenhum arquivo',
'aucune_categorie' =>'Nenhuma categoria',
'aucun_commentaire' =>'Nenhum coment�rio',
'aucune_langue' =>'Nenhuma l�ngua',
'aucun_lien' =>'Nenhum link',
'aucun_trackback' =>'Nenhum trackback',

//C
'calendrier' =>'Calend&aacute;rio',
'categorie' =>'Categoria',
'categories' =>'Categorias',
'commentaire' =>'Coment&aacute;rio',
'commentaires' =>'Coment&aacute;rios',
'commentaires_pour' =>'Coment&aacute;rios para',
'commentaires_fermes' =>'Os coment&aacute;rios para este post est&atilde;o fechados',
'credits' =>'<a href=>"https://www.spip.net/en">feita por SPIP</a>,
<a href=>"https://contrib.spip.net/Spip-Clear">vestido por Spip.Clear</a>',

//D
'date_jour_abbr_1' =>'Dom',
'date_jour_abbr_2' =>'Seg',
'date_jour_abbr_3' =>'Ter',
'date_jour_abbr_4' =>'Qua',
'date_jour_abbr_5' =>'Qui',
'date_jour_abbr_6' =>'Sex',
'date_jour_abbr_7' =>'Sab',
'de' =>'de',

//E
'titre_erreur' =>'Erro :',
'texte_erreur1' =>'A busca por',
'texte_erreur2' =>'n&atilde;o d&aacute; resultado.',

//F
'fil_rss' =>'feed rss',
'fil_rss_commentaires' =>'feed rss coment&aacute;rios',
'fil_atom' =>'feed atom',
'fil_atom_commentaires' =>'feed atom coment&aacute;rios',

//G
'go_main' =>'Ir para o conte&uacute;do',
'go_sidebar' =>'V&aacute; ao menu',
'go_search' =>'V&aacute; para busca',

//H
'hebergement' =>'hospedado por',

//L
'langue' =>'linguagem',
'langues' =>'l&iacute;nguas',
'lien' =>'Link',
'liens' =>'Links',

//O
'oksearch' =>'ok',

//P
'permalink_pour' =>'Link permanente para',

//R
'resultat_recherche' =>'Resultados para a sua busca de',
'rss_pour' =>'Feed RSS coment&aacute;rios para',

//S
'selection' =>'selec&ccedil;&atilde;o',
'site_comment' =>'site',
'syndication' =>'Syndica&ccedil;&atilde;o',

//T
'trackback' =>'trackback',
'trackbacks' =>'trackbacks',
'trackbacks_pour_faire' =>'Para fazer um trackback para este post',
'trackbacks_fermes' =>'Os trackbacks para este post est�o fechados.',

);

?>